# Prometheus am executor docker image

This repo provides a really slim img, about 18 MB, with a version of prometheus am executor. The version can be changed providing the `VERSION` argument at build.

Config for the executor **needs** to be provided at `/etc/prometheus-am-executor/config.yml` as a volume. You could also set environment variables and it will get them.

Refer to [the official repository](https://github.com/imgix/prometheus-am-executor) to see more details about configuration.

## Authors

By MrMilú DevOps team
