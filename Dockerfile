FROM golang:1.16-buster as builder

ARG VERSION=v2.0.2

RUN apt update && apt install -y git gcc musl-dev libc-dev && git clone --depth 1 -b ${VERSION} https://github.com/imgix/prometheus-am-executor.git /prom-exec

RUN cd /prom-exec && go test -count 1 -v . && go build

FROM python:3.8

RUN pip3 install psycopg2 asyncssh asyncio boto3 botocore python-logging-loki

COPY --from=builder /prom-exec/prometheus-am-executor /usr/local/bin/prometheus-am-executor

WORKDIR /etc/prometheus-am-executor/

ENTRYPOINT [ "prometheus-am-executor", "-f",  "config.yml" ]